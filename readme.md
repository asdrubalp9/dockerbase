# Plantilla base de docker <h1>

Este es un docker basico con nginx, php y mysql para poder montarle un ambiente de desarrollo local en la PC.

para correr comandos de artisan:

> **docker-compose exec php php /var/www/html/artisan**

> explicacion del comando de arriba, docker-compose es el cli de docker, exec para ejecutar comandos, php es por el nombre en el archivo.yml, php es para referenciar php dentro del contenedor y la ruta completa de artisan.

**nota:** Probablemente toca agregar una carpeta para mysql

## **colocar todo el contenido de la pagina dentro de la carpeta SRC.** <h2>

## **Cuando es la primera vez que se corre esto:** <h2>
    > docker-compose build && docker-compose up

## **Cuado se quiere correr de nuevo** Pero va a ocupar el terminar <h2>
    > docker-compose up

## **Cuado se quiere correr de nuevo** Pero no se quiere ocupar el terminal  (el -d es dettached mode)<h2>
    > docker-compose up -d

## **Si esta en dettached mode y quieres cerrar docker**  <h2>
    > docker-compose down

### **Fuentes** <h3>

> https://dev.to/aschmelyun/the-beauty-of-docker-for-local-laravel-development-13c0

> https://www.youtube.com/watch?v=5N6gTVCG_rw